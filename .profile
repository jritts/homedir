case "$OSTYPE" in
  darwin*)  platform='mac' ;;
  linux*)   platform='linux' ;;
  msys*)    platform='win' ;;
  *)        platform='unknown' ;;
esac

# --------------------------------------------------
# Linux

if [[ $platform == 'linux' ]]; then
  export TERM=xterm-256color

  if [ -n "$BASH_VERSION" ]; then
    if [ -f "$HOME/.bashrc" ]; then
      . "$HOME/.bashrc"
    fi
  fi
  if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
  fi
  if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
  fi

  alias rmdsstore="find . -name '.DS_Store' -type f -delete"
  alias f='nautilus . &'
  alias byebye='systemctl suspend'

  echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
fi

# --------------------------------------------------
# Windows

if [[ $platform == 'win' ]]; then
  alias e='explorer'
  alias f='explorer'
fi

# --------------------------------------------------
# OSX

if [[ $platform == 'mac' ]]; then
  export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
  alias rmdsstore="find . -name '.DS_Store' -type f -delete"
  alias f='open'
  alias git=/opt/homebrew/bin/git
  alias launch="/Applications/Xcode.app/Contents/MacOS/Xcode all.xcodeproj &"

  export CLICOLOR=1
fi

# --------------------------------------------------
# Common

alias l='ls -FGlAhp'

alias ~="cd ~"

alias pu='pushd'
alias po='popd'

alias mkdir='mkdir -pv'

alias h='history'

alias path='echo -e ${PATH//:/\\n}'

alias ai='adb install -r'
alias as='adb shell'
alias aps='adb shell ps | grep'

alias gs='git status'
alias gl='git log'
alias gfa='git fetch --all'
alias gd='git diff'
alias gdc='git diff --cached'
alias gsh='git show'
alias gshno='git show --name-only'
alias gundo='git reset HEAD~'
alias ga='git add'
alias gb='git branch'
alias gbr='git branch -r'
alias gsquash='git rebase -i HEAD~$1'
alias grbc='git rebase --continue'
alias gcm='git commit -m'
alias gcp='git cherry-pick -x'
alias gcpnc='git cherry-pick --no-commit'
alias gcsm='git commit -m "Update submodules"'
alias gpr='git pull --rebase'
alias gpo='git push'
alias gsmu='git submodule update --init --recursive --remote'
gsm() { echo "Entering '.'" ; "$@" ; git submodule foreach --recursive "$@" ; }

alias cp='cp -va'
alias mv='mv -v'

alias fif='function _fif() { grep -rn "$1" -e "$2"; }; _fif'
alias fifi='function _fif() { grep -rni "$1" -e "$2"; }; _fif'
alias ff='function _ff() { find . | grep "$1"; }; _ff'
alias ffi='function _ff() { find . | grep -i "$1"; }; _ff'

alias vs='python ../launch.py -v all.sln'
alias xc='python ../launch.py -x all.xcworkspace'

# ------------------------------------------------------

cd ~/src/haiba
clear
echo "Run 'source activate.sh' to set the Haiba environment..."
# source activate.sh
export PATH="/opt/homebrew/anaconda3/bin:$PATH"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
